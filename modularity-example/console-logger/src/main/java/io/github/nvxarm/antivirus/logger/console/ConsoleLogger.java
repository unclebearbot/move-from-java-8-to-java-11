package io.github.nvxarm.antivirus.logger.console;

import io.github.nvxarm.antivirus.io.IoManager;
import io.github.nvxarm.antivirus.logger.AntivirusLogger;
import io.github.nvxarm.antivirus.logger.Tag;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class ConsoleLogger implements AntivirusLogger {
    private OutputStream systemOut = IoManager.getSystemOut();
    private OutputStream systemErr = IoManager.getSystemErr();

    public ConsoleLogger() throws Exception {
        IoManager.requireReady();
        PipedInputStream out = new PipedInputStream();
        Thread outListener = new Thread(() -> {
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(out))) {
                while (true) {
                    info(bufferedReader.readLine());
                }
            } catch (IOException ignore) {
            }
        });
        outListener.setDaemon(true);
        outListener.start();
        IoManager.setSystemOut(new PipedOutputStream(out));
        PipedInputStream err = new PipedInputStream();
        Thread errListener = new Thread(() -> {
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(err))) {
                while (true) {
                    error(bufferedReader.readLine());
                }
            } catch (IOException ignore) {
            }
        });
        errListener.setDaemon(true);
        errListener.start();
        IoManager.setSystemErr(new PipedOutputStream(err));
    }

    @Override
    public void info(Object object) {
        Optional.ofNullable(systemOut).ifPresent(out -> {
            try {
                out.write((getLogPrefix(Tag.INFO) + object + System.lineSeparator()).getBytes(StandardCharsets.UTF_8));
                out.flush();
            } catch (Exception ignore) {
            }
        });
    }

    @Override
    public void error(Object object) {
        Optional.ofNullable(systemErr).ifPresent(err -> {
            try {
                err.write((getLogPrefix(Tag.ERROR) + object + System.lineSeparator()).getBytes(StandardCharsets.UTF_8));
                err.flush();
            } catch (Exception ignore) {
            }
        });
    }

    private String getLogPrefix(Tag tag) {
        return String.format("%-8s %5s - ", LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")), tag.getDisplayName());
    }
}
