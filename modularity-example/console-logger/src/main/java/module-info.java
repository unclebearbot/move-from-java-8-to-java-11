module io.github.nvxarm.antivirus.logger.console {
    requires io.github.nvxarm.antivirus.logger;
    provides io.github.nvxarm.antivirus.logger.AntivirusLogger with io.github.nvxarm.antivirus.logger.console.ConsoleLogger;
    requires io.github.nvxarm.antivirus.io;
}
