package io.github.nvxarm.antivirus.ui.web;

import com.sun.net.httpserver.HttpServer;
import io.github.nvxarm.antivirus.io.IoManager;
import io.github.nvxarm.antivirus.ui.AntivirusUi;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

public class WebUi {
    public static AntivirusUi provider() {
        return new AntivirusUi() {
            private HttpServer httpServer;
            private ByteArrayOutputStream output;
            private InputStream stdout;
            private InputStream stderr;
            private OutputStream stdin;

            @Override
            public void launch() {
                try {
                    output = new ByteArrayOutputStream();
                    IoManager.requireReady();
                    IoManager.registerOut(output);
                    httpServer = newHttpServer();
                    httpServer.start();
                    new ProcessBuilder().command("cmd", "/c", "start", "http://127.0.0.1:8080/antivirus.html").start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void exit() {
                Optional.ofNullable(httpServer).ifPresent(it -> it.stop(0));
                Stream.of(stdout, stderr, stdin).filter(Objects::nonNull).forEach(closeable -> {
                    try {
                        closeable.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }

            @Override
            public InputStream stdout() {
                return stdout;
            }

            @Override
            public InputStream stderr() {
                return stderr;
            }

            @Override
            public OutputStream stdin() {
                return stdin;
            }

            private HttpServer newHttpServer() throws Exception {
                HttpServer httpServer = HttpServer.create(new InetSocketAddress(8080), 0);
                httpServer.createContext("/api/antivirus/status", httpExchange -> {
                    httpExchange.getResponseHeaders().add("content-type", "text/plain;charset=utf-8");
                    httpExchange.sendResponseHeaders(200, 0);
                    OutputStream responseBody = httpExchange.getResponseBody();
                    responseBody.write(output.toByteArray());
                    responseBody.flush();
                    responseBody.close();
                });
                httpServer.createContext("/antivirus.html", httpExchange -> {
                    httpExchange.getResponseHeaders().add("content-type", "text/html;charset=utf-8");
                    httpExchange.sendResponseHeaders(200, 0);
                    OutputStream responseBody = httpExchange.getResponseBody();
                    responseBody.write("<html><head><title>Antivirus Web UI</title><script type=\"text/javascript\">setInterval(()=>fetch('/api/antivirus/status').then(response=>response.text()).then(text=>document.getElementById(\"status\").innerHTML=text),200);</script></head><body><pre id=\"status\"></pre><br/></body></html>".getBytes(StandardCharsets.UTF_8));
                    responseBody.flush();
                    responseBody.close();
                });
                return httpServer;
            }
        };
    }
}
