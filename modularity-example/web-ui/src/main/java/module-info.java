module io.github.nvxarm.antivirus.ui.web {
    requires io.github.nvxarm.antivirus.ui;
    provides io.github.nvxarm.antivirus.ui.AntivirusUi with io.github.nvxarm.antivirus.ui.web.WebUi;
    requires io.github.nvxarm.antivirus.io;
    requires jdk.httpserver;
}
