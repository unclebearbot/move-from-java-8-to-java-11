package io.github.nvxarm.antivirus.logger.file;

import io.github.nvxarm.antivirus.io.IoManager;
import io.github.nvxarm.antivirus.logger.AntivirusLogger;
import io.github.nvxarm.antivirus.logger.Tag;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class FileLogger implements AntivirusLogger {
    private Path path = Files.createTempDirectory("Antivirus-");
    private FileOutputStream stdout = new FileOutputStream(new File(path.toFile(), "stdout.log"));
    private FileOutputStream stderr = new FileOutputStream(new File(path.toFile(), "stderr.log"));

    public FileLogger() throws Exception {
        new ProcessBuilder().command("cmd", "/c", "start", path.toAbsolutePath().toString()).start();
        IoManager.requireReady();
        PipedInputStream out = new PipedInputStream();
        Thread outListener = new Thread(() -> {
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(out))) {
                while (true) {
                    info(bufferedReader.readLine());
                }
            } catch (IOException ignore) {
            }
        });
        outListener.setDaemon(true);
        outListener.start();
        IoManager.registerOut(new PipedOutputStream(out));
        PipedInputStream err = new PipedInputStream();
        Thread errListener = new Thread(() -> {
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(err))) {
                while (true) {
                    error(bufferedReader.readLine());
                }
            } catch (IOException ignore) {
            }
        });
        errListener.setDaemon(true);
        errListener.start();
        IoManager.registerErr(new PipedOutputStream(err));
    }

    @Override
    public void info(Object object) {
        Optional.ofNullable(stdout).ifPresent(out -> {
            try {
                out.write((getLogPrefix(Tag.INFO) + object + System.lineSeparator()).getBytes(StandardCharsets.UTF_8));
            } catch (Exception ignore) {
            }
        });
    }

    @Override
    public void error(Object object) {
        Optional.ofNullable(stderr).ifPresent(err -> {
            try {
                err.write((getLogPrefix(Tag.ERROR) + object + System.lineSeparator()).getBytes(StandardCharsets.UTF_8));
            } catch (Exception ignore) {
            }
        });
    }

    private String getLogPrefix(Tag tag) {
        return String.format("%-23s %5s - ", ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z")), tag.getDisplayName());
    }
}
