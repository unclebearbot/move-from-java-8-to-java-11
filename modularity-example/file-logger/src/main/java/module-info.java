module io.github.nvxarm.antivirus.logger.file {
    requires io.github.nvxarm.antivirus.logger;
    provides io.github.nvxarm.antivirus.logger.AntivirusLogger with io.github.nvxarm.antivirus.logger.file.FileLogger;
    requires io.github.nvxarm.antivirus.io;
}
