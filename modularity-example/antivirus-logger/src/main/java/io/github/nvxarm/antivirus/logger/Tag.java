package io.github.nvxarm.antivirus.logger;

public enum Tag {
    INFO("INFO"),
    ERROR("ERROR");

    private final String displayName;

    Tag(String displayName) {
        this.displayName = displayName;

    }

    public String getDisplayName() {
        return displayName;
    }
}
