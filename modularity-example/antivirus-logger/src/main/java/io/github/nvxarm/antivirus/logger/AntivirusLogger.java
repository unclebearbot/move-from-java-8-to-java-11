package io.github.nvxarm.antivirus.logger;

public interface AntivirusLogger {
    void info(Object object);

    void error(Object object);
}
