module io.github.nvxarm.antivirus.engine.kaspersky {
    requires io.github.nvxarm.antivirus.engine;
    provides io.github.nvxarm.antivirus.engine.AntivirusEngine with io.github.nvxarm.antivirus.engine.kaspersky.KasperskyEngine;
}
