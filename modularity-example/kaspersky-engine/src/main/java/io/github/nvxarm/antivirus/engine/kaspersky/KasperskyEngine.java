package io.github.nvxarm.antivirus.engine.kaspersky;

import io.github.nvxarm.antivirus.engine.AntivirusEngine;

public class KasperskyEngine implements AntivirusEngine {
    @Override
    public void launch() {
        delay();
        System.out.println("Kaspersky engine is launched");
        delay();
    }

    @Override
    public void exit() {
        delay();
        System.out.println("Kaspersky engine is exiting");
        delay();
    }
}
