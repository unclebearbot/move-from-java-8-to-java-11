package io.github.nvxarm.antivirus.engine;

public interface AntivirusEngine {
    void launch();

    void exit();

    default void delay() {
        delay((long) (3000 + 2000 * Math.random()));
    }

    default void delay(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
    }
}
