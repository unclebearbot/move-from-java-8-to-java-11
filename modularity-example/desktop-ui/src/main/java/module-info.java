module io.github.nvxarm.antivirus.ui.desktop {
    requires io.github.nvxarm.antivirus.ui;
    provides io.github.nvxarm.antivirus.ui.AntivirusUi with io.github.nvxarm.antivirus.ui.desktop.DesktopUi;
    requires io.github.nvxarm.antivirus.io;
}
