package io.github.nvxarm.antivirus.ui.desktop;

import io.github.nvxarm.antivirus.io.IoManager;
import io.github.nvxarm.antivirus.ui.AntivirusUi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

public class DesktopUi {
    public static AntivirusUi provider() {
        return new AntivirusUi() {
            private Process process;
            private InputStream stdout;
            private InputStream stderr;
            private OutputStream stdin;

            @Override
            public void launch() {
                try {
                    Path path = Files.createTempFile(null, null);
                    process = new ProcessBuilder().command("cmd", "/c", "start", "Antivirus Desktop UI", "tail", "-f", path.toAbsolutePath().toString()).start();
                    stdout = process.getInputStream();
                    stderr = process.getErrorStream();
                    stdin = process.getOutputStream();
                    IoManager.requireReady();
                    File file = path.toFile();
                    IoManager.registerOut(new FileOutputStream(file));
                    file.deleteOnExit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void exit() {
                Stream.of(stdout, stderr, stdin).filter(Objects::nonNull).forEach(closeable -> {
                    try {
                        closeable.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                Optional.ofNullable(process).ifPresent(Process::destroy);
            }

            @Override
            public InputStream stdout() {
                return stdout;
            }

            @Override
            public InputStream stderr() {
                return stderr;
            }

            @Override
            public OutputStream stdin() {
                return stdin;
            }
        };
    }
}
