package io.github.nvxarm.antivirus.io;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

public class MultiplyInputStream extends InputStream {
    private List<InputStream> inputStreams = new CopyOnWriteArrayList<>();

    public void register(InputStream inputStream) {
        Optional.ofNullable(inputStream).ifPresent(inputStreams::add);
    }

    public void cancel(InputStream inputStream) {
        Optional.ofNullable(inputStream).ifPresent(inputStreams::remove);
    }

    @Override
    public int read() {
        return inputStreams.stream().parallel().filter(inputStream -> {
            try {
                return inputStream.available() > 0;
            } catch (Exception ignore) {
                return false;
            }
        }).findFirst().map(inputStream -> {
            try {
                return inputStream.read();
            } catch (Exception ignore) {
                return -1;
            }
        }).orElse(-1);
    }

    @Override
    public void close() {
        System.err.println("Keeping alive despite of a closing call from " + Optional.of(StackWalker.Option.RETAIN_CLASS_REFERENCE)
                .map(StackWalker::getInstance)
                .map(StackWalker::getCallerClass)
                .map(Class::toGenericString)
                .orElse(null));
    }
}
