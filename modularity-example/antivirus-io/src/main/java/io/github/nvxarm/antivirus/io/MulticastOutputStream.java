package io.github.nvxarm.antivirus.io;

import java.io.OutputStream;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

public class MulticastOutputStream extends OutputStream {
    private List<OutputStream> outputStreams = new CopyOnWriteArrayList<>();

    public void register(OutputStream outputStream) {
        Optional.ofNullable(outputStream).ifPresent(outputStreams::add);
    }

    public void cancel(OutputStream outputStream) {
        Optional.ofNullable(outputStream).ifPresent(outputStreams::remove);
    }

    @Override
    public void write(int b) {
        outputStreams.stream().parallel().forEach(outputStream -> {
            try {
                outputStream.write(b);
                outputStream.flush();
            } catch (Exception ignore) {
            }
        });
    }

    @Override
    public void close() {
        System.err.println("Keeping alive despite of a closing call from " + Optional.of(StackWalker.Option.RETAIN_CLASS_REFERENCE)
                .map(StackWalker::getInstance)
                .map(StackWalker::getCallerClass)
                .map(Class::toGenericString)
                .orElse(null));
    }
}
