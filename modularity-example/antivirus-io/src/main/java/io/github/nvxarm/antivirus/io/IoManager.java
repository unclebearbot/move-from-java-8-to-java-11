package io.github.nvxarm.antivirus.io;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Optional;

public class IoManager {
    private static OutputStream systemOut = System.out;
    private static OutputStream systemErr = System.err;
    private static InputStream systemIn = System.in;
    private static MulticastOutputStream out = new MulticastOutputStream();
    private static MulticastOutputStream err = new MulticastOutputStream();
    private static MultiplyInputStream in = new MultiplyInputStream();
    private static boolean ready = false;

    public static void requireReady() throws Exception {
        if (ready) {
            return;
        }
        init();
    }

    private static synchronized void init() throws Exception {
        if (ready) {
            return;
        }
        initSystemIo();
    }

    private static synchronized void initSystemIo() throws Exception {
        if (ready) {
            throw new Exception(new IllegalStateException());
        }
        registerOut(systemOut);
        System.setOut(new PrintStream(out));
        registerErr(systemErr);
        System.setErr(new PrintStream(err));
        registerIn(systemIn);
        System.setIn(in);
        ready = true;
    }

    public static OutputStream stdout() {
        return out;
    }

    public static OutputStream stderr() {
        return err;
    }

    public static InputStream stdin() {
        return in;
    }

    public static void registerOut(OutputStream outputStream) {
        Optional.ofNullable(outputStream).ifPresent(out::register);
    }

    public static void registerErr(OutputStream outputStream) {
        Optional.ofNullable(outputStream).ifPresent(err::register);
    }

    public static void registerIn(InputStream inputStream) {
        Optional.ofNullable(inputStream).ifPresent(in::register);
    }

    public static OutputStream getSystemOut() {
        return systemOut;
    }

    public static void setSystemOut(OutputStream outputStream) {
        registerOut(outputStream);
        out.cancel(systemOut);
        systemOut = outputStream;
    }

    public static OutputStream getSystemErr() {
        return systemErr;
    }

    public static void setSystemErr(OutputStream outputStream) {
        registerErr(outputStream);
        err.cancel(systemErr);
        systemErr = outputStream;
    }

    public static InputStream getSystemIn() {
        return systemIn;
    }

    public static void setSystemIn(InputStream inputStream) {
        registerIn(inputStream);
        in.cancel(systemIn);
        systemIn = inputStream;
    }
}
