package io.github.nvxarm.antivirus.engine.avira;

import io.github.nvxarm.antivirus.engine.AntivirusEngine;

public class AviraEngine implements AntivirusEngine {
    @Override
    public void launch() {
        delay();
        System.out.println("Avira engine is launched");
        delay();
    }

    @Override
    public void exit() {
        delay();
        System.out.println("Avira engine is exiting");
        delay();
    }
}
