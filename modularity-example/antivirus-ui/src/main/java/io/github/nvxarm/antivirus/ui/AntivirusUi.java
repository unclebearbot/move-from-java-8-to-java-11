package io.github.nvxarm.antivirus.ui;

import java.io.InputStream;
import java.io.OutputStream;

public interface AntivirusUi {
    void launch();

    void exit();

    InputStream stdout();

    InputStream stderr();

    OutputStream stdin();
}
