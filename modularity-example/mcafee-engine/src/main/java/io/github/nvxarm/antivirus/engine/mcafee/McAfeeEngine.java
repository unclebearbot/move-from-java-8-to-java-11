package io.github.nvxarm.antivirus.engine.mcafee;

import io.github.nvxarm.antivirus.engine.AntivirusEngine;

public class McAfeeEngine implements AntivirusEngine {
    @Override
    public void launch() {
        delay();
        System.out.println("McAfee engine is launched");
        delay();
    }

    @Override
    public void exit() {
        delay();
        System.out.println("McAfee engine is exiting");
        delay();
    }
}
