module io.github.nvxarm.antivirus.engine.mcafee {
    requires io.github.nvxarm.antivirus.engine;
    provides io.github.nvxarm.antivirus.engine.AntivirusEngine with io.github.nvxarm.antivirus.engine.mcafee.McAfeeEngine;
}
