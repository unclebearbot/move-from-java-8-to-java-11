import io.github.nvxarm.antivirus.logger.AntivirusLogger;

module io.github.nvxarm.antivirus.app {
    requires io.github.nvxarm.antivirus.logger;
    uses AntivirusLogger;
    requires io.github.nvxarm.antivirus.engine;
    uses io.github.nvxarm.antivirus.engine.AntivirusEngine;
    requires io.github.nvxarm.antivirus.ui;
    uses io.github.nvxarm.antivirus.ui.AntivirusUi;
}
