package io.github.nvxarm.antivirus.app;

import io.github.nvxarm.antivirus.engine.AntivirusEngine;
import io.github.nvxarm.antivirus.logger.AntivirusLogger;
import io.github.nvxarm.antivirus.ui.AntivirusUi;

import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Collectors;

public class AntivirusApp {
    private List<AntivirusEngine> antivirusEngines;
    private List<AntivirusUi> antivirusUis;
    private List<AntivirusLogger> antivirusLoggers;

    public static void main(String[] args) {
        AntivirusApp antivirusApp = new AntivirusApp();
        antivirusApp.init();
        antivirusApp.launch();
        antivirusApp.exit();
    }

    private void init() {
        antivirusEngines = ServiceLoader.load(AntivirusEngine.class).stream().map(ServiceLoader.Provider::get).collect(Collectors.toList());
        antivirusUis = ServiceLoader.load(AntivirusUi.class).stream().map(ServiceLoader.Provider::get).collect(Collectors.toList());
        antivirusLoggers = ServiceLoader.load(AntivirusLogger.class).stream().map(ServiceLoader.Provider::get).collect(Collectors.toList());
        if (antivirusEngines.isEmpty()) {
            System.out.println("No antivirus engine has been loaded");
        }
        if (antivirusUis.isEmpty()) {
            System.out.println("No antivirus UI has been loaded");
        }
        if (antivirusLoggers.isEmpty()) {
            System.out.println("No antivirus logger has been loaded");
        }
    }

    private void launch() {
        antivirusUis.parallelStream().forEach(AntivirusUi::launch);
        antivirusEngines.parallelStream().forEach(AntivirusEngine::launch);
    }

    private void exit() {
        antivirusEngines.parallelStream().forEach(AntivirusEngine::exit);
        antivirusUis.parallelStream().forEach(AntivirusUi::exit);
    }
}
